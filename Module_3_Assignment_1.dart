import 'package:flutter/material.dart';
import 'package:flutter/journal.dart';
import 'package:flutter/settings.dart';
import 'package:flutter/profile.dart';

void main() {
  runApp(
    MaterialApp(
      home: MyApp(
      ),
    ));
}



class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("About Me"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: new MaterialButton(
                    height: 100.0,
                    minWidth: 150.0
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.green,
                  child: new Text("Profile"),
                  onPressed: () => {
                    Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => Profile()),
                    )
                    },
                    splashColor: Colors.yellowAccent,
                  )),

                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: new MaterialButton(
                    height: 100.0,
                    minWidth: 150.0
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.green,
                  child: new Text("check-in"),
                  onPressed: () => {
                    Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => Checkin()),
                    )
                    },
                    splashColor: Colors.yellowAccent,
                  )),
                  
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: new MaterialButton(
                    height: 100.0,
                    minWidth: 150.0
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.green,
                  child: new Text("Settings"),
                  onPressed: () => {
                    Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => Settings()),
                    )
                    },
                    splashColor: Colors.yellowAccent,
                  )),  
                 Padding(
                  padding: EdgeInsets.all(20.0),
                  child: new MaterialButton(
                    height: 100.0,
                    minWidth: 150.0
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.green,
                  child: new Text("Journal"),
                  onPressed: () => {
                    Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => Journal()),
                    )
                    },
                    splashColor: Colors.yellowAccent,
                  )), 
              ],
              ),
          ],
        )),
    );
  }
}

